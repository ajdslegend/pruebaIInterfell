/* eslint-disable quotes */
import Vue from 'vue'

const DOMAIN = 'http://localhost:8000/api/'
var global
global = new Vue({
  data () {
    return {
      LOGIN: DOMAIN + 'login',
      REGISTER: DOMAIN + 'register',
      PROFILE: DOMAIN + 'profile',
      PROFILE_UPDATE: DOMAIN + 'profile/update',
      ACADEMIC: DOMAIN + 'level-academic/read',
      COUNTRY: DOMAIN + 'country/read',
      CHANGE_PASSWORD: DOMAIN + 'reset-password',
      FORGOT_PASSWORD: DOMAIN + 'forgot-password',
      RESET_TOKEN_PASSWORD: DOMAIN + 'reset-token-password',
      getBase64ImageEncode (img) {
        var promise = new Promise((resolve, reject) => {
          let baseString = ''
          var reader = new FileReader()
          reader.readAsDataURL(img)
          reader.onload = function () {
            baseString = reader.result
            return resolve(baseString)
          }
          reader.onerror = function (error) {
            console.log('Error: ', error)
            // return reject()
          }
        })
        return promise
      }
    }
  }
})

export default global
