/* eslint-disable import/first,camelcase,eol-last */
// window._ = require('lodash')
//
//
// window.axios = require('axios')
//
// window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
//
// var access_token = localStorage.getItem('token');
// if (access_token) {
//   window.axios.defaults.headers.common['Authorization'] = 'Bearer ' + access_token
// } else {
//   console.log('empty tokens')
// }

window._ = require('lodash')

import axios from 'axios'

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
var access_token = localStorage.getItem('token')
if (access_token) {
  axios.defaults.headers.common['Authorization'] = 'Bearer ' + access_token
} else {
  console.log('empty tokens')
}
