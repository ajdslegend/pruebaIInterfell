import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Login from '@/components/Login'
import Register from '@/components/Register'
import Auth from '@/components/Auth'
import Profile from '@/components/Profile'
import store from '../utility/store'
import Logout from '@/components/Logout'
import ChangePassword from '@/components/ChangePassword'
import ForgotPassword from '@/components/ForgotPassword'
import ResetPassword from '@/components/ResetPassword'

Vue.use(Router)

const routes = [
  {
    path: '/',
    redirect: {path: '/main/home'}
  },
  {
    path: '/main',
    name: 'main',
    component: Auth,
    children: [
      {
        path: 'profile',
        name: 'profile',
        component: Profile
      },
      {
        path: 'home',
        name: 'home',
        component: Home
      },
      {
        path: 'change-password',
        name: 'change-password',
        component: ChangePassword
      }
    ],
    meta: {requiresAuth: true}
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/forgot-password',
    name: 'forgot-password',
    component: ForgotPassword
  },
  {
    path: '/reset-password/:token',
    component: ResetPassword
  },
  {
    path: '/logout',
    name: 'logout',
    component: Logout
  },
  {
    path: '/register',
    name: 'register',
    component: Register
  }
]

const router = new Router({
  mode: 'history',
  routes
})

router.beforeEach((to, from, next) => {
  // check if the route requires authentication and user is not logged in
  if (to.matched.some(route => route.meta.requiresAuth) && !store.state.isLoggedIn) {
    // redirect to login page
    next({path: '/login'})
    return
  }
  // if logged in redirect to dashboard
  if (to.path === '/login' && store.state.isLoggedIn) {
    next({path: '/main/home'})
    return
  }

  if (to.path === '/register' && store.state.isLoggedIn) {
    next({path: '/main/home'})
    return
  }

  next()
})

export default router
