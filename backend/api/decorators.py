import jwt
import json
from django.http import HttpResponse
from api.models import Users
from backend.settings import SECRET_KEY

allow = '*'
contentType = "application/json;charset=utf-8;"
charset = 'utf-8'


def jwt_login_required(f):
    def wrap(request, *args, **kwargs):
        try:
            try:
                data = request.META.get('HTTP_AUTHORIZATION').encode('utf-8', 'ignore').decode(charset)
                tokenAuth = str.replace(str(data), 'Bearer ', '')
                token = jwt.decode(tokenAuth, SECRET_KEY)
                ac = Users.objects.get(email=token.get("email"))
                if ac:
                    return f(request, *args, **kwargs)
            except Exception:
                data = {'success': False, 'message': "Token inválido", 'data': None, 'exception': "error_exception"}
                Response = HttpResponse(json.dumps(data), status=403, content_type=contentType)
                Response["Access-Control-Allow-Origin"] = allow
                return Response
        except jwt.ExpiredSignature:
            data = {'success': False, 'message': "Token inválido", 'data': None, 'exception': "error_exception"}
            Response = HttpResponse(json.dumps(data), status=403, content_type=contentType)
            Response["Access-Control-Allow-Origin"] = allow
            return Response

    wrap.__doc__ = f.__doc__
    wrap.__name__ = f.__name__
    return wrap
