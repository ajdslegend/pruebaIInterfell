import json

import jwt
from django.http import HttpResponse

from api.models import Users
from backend.settings import SECRET_KEY

allow = '*'
contentType = "application/json;charset=utf-8;"
charset = 'utf-8'

def AuthUser(request):
    try:
        data = request.META.get('HTTP_AUTHORIZATION').encode('utf-8', 'ignore').decode(charset)
        tokenAuth = str.replace(str(data), 'Bearer ', '')
        token = jwt.decode(tokenAuth, SECRET_KEY)
        ac = Users.objects.get(email=token.get("email"))
        return ac
    except Exception:
        return None

def requestData(request):
    return request.body.decode('utf-8')

def responseData(data,status):
    Response = HttpResponse(json.dumps(data), status=status, content_type=contentType)
    Response["Access-Control-Allow-Origin"] = allow
    return Response