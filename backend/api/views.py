import json
from datetime import datetime, timedelta

import jwt
from django.contrib.auth.hashers import BCryptSHA256PasswordHasher as mkBcrypt
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view

from django.core.mail import EmailMessage

from api.decorators import jwt_login_required
from api.models import Users, Country, LevelAcademic, UserLevelAcademic, UserCountry
from api.util import requestData, responseData, AuthUser
from backend.settings import SECRET_KEY,DOMAIN_CLIENT, EMAIL_HOST_USER


@api_view(['GET'])
@csrf_exempt
def index(request):
    data = {'success': True, 'message': "ok", 'data': None, 'exception': None}
    return responseData(data, 200)


@api_view(['POST'])
@csrf_exempt
def login(request):
    try:
        payload = json.loads(requestData(request))
        ac = Users.objects.get(email=payload["email"])
        hasher = mkBcrypt()
        ps = hasher.verify(payload["password"], ac.password)
        if ps:
            usr = {
                'username': ac.username,
                'email': ac.email,
                'exp': datetime.utcnow() + timedelta(seconds=50000)
            }
            token = jwt.encode(usr, SECRET_KEY).decode('utf-8')
            tokens = {'token': (token), 'type': 'Bearer'}
            data = {'success': True, 'message': "ok", 'data': tokens, 'exception': None}
            return responseData(data, 200)
        else:
            data = {'success': False, 'message': "Error de usuario/clave. Verfique bien los datos de entrada",
                    'data': None,
                    'exception': "error_exception"}
            return responseData(data, 200)
    except Exception:
        data = {'success': False, 'message': "Error usuario/clave no existe.Verifique bien los datos de entrada",
                'data': None, 'exception': "error_exception"}
        return responseData(data, 200)


@api_view(['POST'])
@csrf_exempt
def register(request):
    try:
        r = (requestData(request))
        inputs = json.loads(r)
        firstName = inputs["first_name"]
        lastName = inputs["last_name"]
        name = inputs["username"]
        email = inputs["email"]
        passw = inputs["password"]
        LevelAcademicID = inputs["level_academic_id"]
        countryID = inputs["country_id"]
        address = inputs["address"]
        avatar = inputs["avatar"]
        city = inputs["city"]
        hasher = mkBcrypt()
        sl = hasher.salt()
        passNew = hasher.encode(passw, salt=sl)

        us = Users(avatar=avatar, first_name=firstName, last_name=lastName, username=name, email=email,
                   password=passNew,
                   address=address, city=city)
        us.save()

        la = UserLevelAcademic(user_id=us.id, level_academic_id=LevelAcademicID)
        la.save()

        c = UserCountry(user_id=us.id, country_id=countryID)
        c.save()

        data = {'success': True, 'message': "Se registraron los datos exitosamente", 'data': None,
                'exception': None}
        return responseData(data, 200)
    except Exception:
        data = {'success': False, 'message': "error al registrar los datos", 'data': None,
                'exception': "error_exception"}
        return responseData(data, 200)


@api_view(['GET'])
@jwt_login_required
@csrf_exempt
def auth(request):
    try:
        data = {'success': True, 'message': "Se autentico exitosamente", 'data': None,
                'exception': None}
        return responseData(data, 200)
    except Exception:
        data = {'success': False, 'message': "error al autenticar datos", 'data': None,
                'exception': "error_exception"}
        return responseData(data, 200)


@api_view(['POST'])
@csrf_exempt
def register_country(request):
    try:
        r = (requestData(request))
        inputs = json.loads(r)
        country_name = inputs["country"]
        obj = Country(country=country_name)
        obj.save()
        data = serializers.serialize('json', [obj, ])
        decode_json = json.loads(data)
        encode_json = (decode_json[0])
        data = {'success': True, 'message': "Se registro exitosamente", 'data': encode_json,
                'exception': None}
        return responseData(data, 200)
    except Exception:
        data = {'success': False, 'message': "error al registrar los datos", 'data': None,
                'exception': "error_exception"}
        return responseData(data, 200)


@api_view(['POST'])
@csrf_exempt
def register_level_academic(request):
    try:
        r = (requestData(request))
        inputs = json.loads(r)
        name = inputs["name"]
        obj = LevelAcademic(name=name)
        obj.save()
        data = serializers.serialize('json', [obj, ])
        decode_json = json.loads(data)
        encode_json = (decode_json[0])
        data = {'success': True, 'message': "Se registro exitosamente", 'data': encode_json,
                'exception': None}
        return responseData(data, 200)
    except Exception:
        data = {'success': False, 'message': "Error al registar los datos", 'data': None,
                'exception': "error_exception"}
        return responseData(data, 200)


@api_view(['GET'])
@csrf_exempt
def read_country(request):
    try:
        obj = Country.objects.all()
        encode_json = [{'country': rs.country, 'id': rs.id} for rs in obj]
        data = {'success': True, 'message': "ok", 'data': encode_json,
                'exception': None}
        return responseData(data, 200)
    except Exception:
        data = {'success': False, 'message': "Error al obtener los datos", 'data': None,
                'exception': "error_exception"}
        return responseData(data, 200)


@api_view(['GET'])
@csrf_exempt
def read_level_academic(request):
    try:
        obj = LevelAcademic.objects.all()
        encode_json = [{'name': rs.name, 'id': rs.id} for rs in obj]
        data = {'success': True, 'message': "ok", 'data': encode_json,
                'exception': None}
        return responseData(data, 200)
    except Exception:
        data = {'success': False, 'message': "Error al obtener los datos", 'data': None,
                'exception': "error_exception"}
        return responseData(data, 200)


@api_view(['GET'])
@jwt_login_required
@csrf_exempt
def profile(request):
    try:
        au = AuthUser(request)
        lai = UserLevelAcademic.objects.get(user_id=au.id)
        ci = UserCountry.objects.get(user_id=au.id)
        la = LevelAcademic.objects.get(id=lai.level_academic_id)
        c = Country.objects.get(id=ci.country_id)
        dataObject = {
            'avatar': au.avatar,
            'first_name': au.first_name,
            'last_name': au.last_name,
            'username': au.username,
            'email': au.email,
            'address': au.address,
            'city': au.city,
            'country': {
                'id': c.id,
                'country': c.country
            },
            'level_academic': {
                'id': la.id,
                'name': la.name
            }
        }

        encode_json = dataObject
        data = {'success': True, 'message': "ok", 'data': encode_json,
                'exception': None}
        return responseData(data, 200)
    except Exception:
        data = {'success': False, 'message': "Error al obtener los datos", 'data': None,
                'exception': "error_exception"}
        return responseData(data, 200)


@api_view(['POST'])
@jwt_login_required
@csrf_exempt
def reset_password(request):
    try:
        au = AuthUser(request)
        r = (requestData(request))
        inputs = json.loads(r)
        ac = Users.objects.get(email=au.email)
        hasher = mkBcrypt()
        ps = hasher.verify(inputs["old_password"], ac.password)
        if ps:
            sl = hasher.salt()
            passNew = hasher.encode(inputs["password"], salt=sl)
            ac.password = passNew
            ac.save()
            data = {'success': True, 'message': "Se actualizaron los datos exitosamente", 'data': None,
                    'exception': None}
            return responseData(data, 200)
        else:
            data = {'success': False, 'message': "La clave actual no coincide con el registro", 'data': None,
                    'exception': "error_exception"}

            return responseData(data, 200)
    except Exception:
        data = {'success': False, 'message': "error al actualizar los registros", 'data': None,
                'exception': "error_exception"}
        return responseData(data, 200)


@api_view(['POST'])
@jwt_login_required
@csrf_exempt
def update_profile(request):
    try:
        au = AuthUser(request)
        r = (requestData(request))
        inputs = json.loads(r)
        firstName = inputs["first_name"]
        lastName = inputs["last_name"]
        name = inputs["username"]
        email = inputs["email"]
        LevelAcademicID = inputs["level_academic_id"]
        countryID = inputs["country_id"]
        address = inputs["address"]
        avatar = inputs["avatar"]
        city = inputs["city"]
        us = Users.objects.get(id=au.id)
        us.avatar = avatar
        us.first_name = firstName
        us.last_name = lastName
        us.username = name
        us.email = email
        us.address = address
        us.city = city
        us.save()

        la = UserLevelAcademic.objects.get(user_id=au.id)
        la.level_academic_id = LevelAcademicID
        la.save()

        c = UserCountry.objects.get(user_id=au.id)
        c.country_id = countryID
        c.save()

        data = {'success': True, 'message': "Se actualizaron los datos exitosamente", 'data': None,
                'exception': None}
        return responseData(data, 200)
    except Exception:
        data = {'success': False, 'message': "error al actualizar los datos", 'data': None,
                'exception': "error_exception"}
        return responseData(data, 200)


@api_view(['POST'])
@csrf_exempt
def forgotPassword(request):
     try:
        r = (requestData(request))
        inputs = json.loads(r)
        email = inputs["email"]
        usr = Users.objects.get(email=email)
        if usr:
            hasher = mkBcrypt()
            sl = hasher.salt()
            emailHash = hasher.encode(email, salt=sl)
            usrs = {
                'emailHash': emailHash,
                'email': usr.email,
                'exp': datetime.utcnow() + timedelta(seconds=50000)
            }
            token = jwt.encode(usrs, SECRET_KEY).decode('utf-8')
            # tokens = {'token': (token)}
            link = '<a href="'+DOMAIN_CLIENT+'reset-password/'+token+'">link</a>'
            contentHtml = 'click aqui para reset password: ' + link
            send = EmailMessage('Olvido el password',contentHtml ,EMAIL_HOST_USER, [email])
            send.content_subtype = "html"
            send.send()
            message = {'message':contentHtml}
            data = {'success': True, 'message': "Se ha enviado un email. Verifica", 'data': message, 'exception': None}
            return responseData(data, 200)
        else:
            data = {'success': False, 'message': "error al obtener los datos", 'data': None,
                    'exception': "error_exception"}
            return responseData(data, 200)
     except Exception:
        data = {'success': False, 'message': "error al obtener los datos", 'data': None,
                'exception': "error_exception"}
        return responseData(data, 200)


@api_view(['POST'])
@csrf_exempt
def reset_token_password(request):
    try:
        r = (requestData(request))
        inputs = json.loads(r)
        tokenAuth = inputs["token"]
        token = jwt.decode(tokenAuth, SECRET_KEY)
        ac = Users.objects.get(email=token.get("email"))
        hasher = mkBcrypt()
        ps = hasher.verify(ac.email, token.get("emailHash"))
        if ps:
            sl = hasher.salt()
            passNew = hasher.encode(inputs["password"], salt=sl)
            ac.password = passNew
            ac.save()
            data = {'success': True, 'message': "Se actualizaron los datos exitosamente", 'data': None,
                    'exception': None}
            return responseData(data, 200)
        else:
            data = {'success': False, 'message': "La clave actual no coincide con el registro", 'data': None,
                    'exception': "error_exception"}

            return responseData(data, 200)
    except Exception:
        data = {'success': False, 'message': "error al actualizar los registros", 'data': None,
                'exception': "error_exception"}
        return responseData(data, 200)
