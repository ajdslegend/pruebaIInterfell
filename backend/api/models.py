# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Country(models.Model):
    country = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'country'


class LevelAcademic(models.Model):
    name = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'level_academic'


class UserCountry(models.Model):
    user = models.ForeignKey('Users', models.DO_NOTHING, blank=True, null=True)
    country = models.ForeignKey(Country, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'user_country'


class UserLevelAcademic(models.Model):
    user = models.ForeignKey('Users', models.DO_NOTHING, blank=True, null=True)
    level_academic = models.ForeignKey(LevelAcademic, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'user_level_academic'


class Users(models.Model):
    first_name = models.CharField(max_length=250, blank=True, null=True)
    last_name = models.CharField(max_length=250, blank=True, null=True)
    username = models.CharField(max_length=250, blank=True, null=True)
    email = models.CharField(max_length=250, blank=True, null=True)
    password = models.CharField(max_length=250, blank=True, null=True)
    address = models.TextField(blank=True, null=True)
    avatar = models.TextField(blank=True, null=True)
    city = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'users'
        unique_together = (('email', 'username'),)
