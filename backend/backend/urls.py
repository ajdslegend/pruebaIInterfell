"""backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from api import views as v

urlpatterns = [
    path('', v.index),
    path('admin/', admin.site.urls),
    path('api/login', v.login),
    path('api/register', v.register),
    path('api/country/create', v.register_country),
    path('api/country/read', v.read_country),
    path('api/level-academic/read', v.read_level_academic),
    path('api/level-academic/create', v.register_level_academic),
    path('api/profile', v.profile),
    path('api/profile/update', v.update_profile),
    path('api/reset-password', v.reset_password),
    path('api/forgot-password', v.forgotPassword),
    path('api/reset-token-password', v.reset_token_password),
    path('api/auth', v.auth)
]
