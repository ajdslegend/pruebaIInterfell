import Vue from 'vue'
import App from './components/AppComponent'
import router from './router'
import store from './store'
import './registerServiceWorker'

// import App from './App.vue'

require('./bootstrap');

Vue.config.productionTip = false


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
