# Test Interfell #
----------
### Autor: Anderson Delgado ###
### Email: anderson.andersondelgado.delga@gmail.com ###


## Estructura de directorios: ##
- **Backend:** Python Django
- **Backup:** Respaldo de la base de datos
- **Frontend:** VueJS

## Backend ##
### Base de datos ###
### Nombre de la base de datos: ###
- **SGDB:** postgresql
- **Nombre:** `db_interfell_prueba`
- **Usuario:** admin
- **clave:** password
- **HOST:** localhost
- **PORT:** 5432


- **Nota: SGBD (sistema De Gestión de Base de Datos)**
### Django y dependencias ###
- pip install -r requirements.txt
- python manage.py runserver


- **El servidor que ejecutará por defecto con dominio localhost y puerto 8000 es decir: **http://localhost:8000****

## Frontend ##
### Vuejs y dependencias ###
- npm install
- npm run dev


- **Nota: El servidor que ejecutará por defecto con dominio localhost y puerto 8080 es decir:
**http://localhost:8080**** ###


