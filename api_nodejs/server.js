'use strict'

require('dotenv').config();

var cors = require('cors');
var bodyParser = require('body-parser');
var methodOverride = require("method-override");
var
  express = require('express'),
  app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(methodOverride());
app.use(express.static(__dirname));

app.use(cors({
  'origin': '*'
}));

require('./route')(app);

app.listen(process.env.PORT);

console.log("Servidor Express escuchando en modo %s", app.settings.env + '...PORT: ' + process.env.PORT);