'use strict'

module.exports = (sequelize, DataTypes) => {

  const users = sequelize.define('users', {
    id: {type: DataTypes.SMALLINT, primaryKey: true},
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    //created_at: DataTypes.DATE,
    //updated_at:DataTypes.DATE,
    remember_token:DataTypes.STRING,
    email_verified_at:DataTypes.DATE
  });

  return users;

};
