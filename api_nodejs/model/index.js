'use strict'

require('dotenv').config();

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const basename = path.basename(module.filename);
const db = {};

const conn = process.env.DB_CONNECTION;
const user = process.env.DB_USER;
const pass = process.env.DB_PASSWORD;
const host = process.env.DB_HOST;
const port = process.env.DB_PORT;
const dbconn = process.env.DB_DATABASE;
const connection = conn + '://' + user + ':' + pass + '@' + host + ':' + port + '/' + dbconn;

let sequelize;
sequelize = new Sequelize(connection);

// const configs = {
//   username: user,
//   password: pass,
//   database: dbconn,
//   host: host,
//   port: port,
//   dialect: conn,
//   pool: {
//     max: 10,
//     min: 0,
//     acquire: 30000,
//     idle: 10000
//   }
// }

// sequelize = new Sequelize(
//   configs.database, configs.username, configs.password, configs
// );

fs
  .readdirSync(__dirname)
  .filter((file) =>
    (file.indexOf('.') !== 0) &&
    (file !== basename) &&
    (file.slice(-3) === '.js'))
  .forEach((file) => {
    // console.log('#file: '+file);
    const model = sequelize.import(path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach((modelName) => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;