'use strict'

const User = require('../model').users;

var bcrypt = require('bcrypt');
var moment = require('moment');
var jwt = require('jwt-simple');

const saltRounds = 10;

module.exports = {

  index(req, res) {
    res.header('Content-type', 'text/html');
    return res.end('<h1>Hello !</h1>');
  },
  auth(req, res) {
    var token = req.headers.authorization.split(" ")[1];
    var payload = jwt.decode(token, process.env.APP_KEY);

    return User.find({
      attributes: ['name', 'email'],
      where: {email: payload.email}
    }).then((data) => {
      if (data === null) {
        return res
          .status(403)
          .send({success: false, data: null, message: "Incorrect credentials"});
      } else {
        res.status(200).json({
          success: true,
          data: data,
          message: 'ok'
        });
      }
    });
  },
  register(req, res) {
    let password = req.param('password', null);
    let email = req.param('email', null);
    let name = req.param('name', null);
    User.find({where: {email: email}}).then((data) => {
      if (data === undefined || data === null) {
        bcrypt.genSalt(saltRounds, (err, salt) => {
          if (err) return next(err);

          bcrypt.hash(password, salt, (err, hash) => {
            if (err) return next(err);
            password = hash;
            let payload = {
              name: name,
              email: email,
              password: password
            };

            return User.create(payload).then((data) => {
              res.status(200).json({
                success: true,
                data: data,
                message: 'ok'
              });
            }).catch((error) => {
              res.send({
                success: false,
                data: null,
                message: [error.message]
              });
            });

          });
        });
      } else {
        res.send({
          success: false,
          data: null,
          message: ['recorset isset']
        });
      }
    }).catch((error) => {
      res.send({
        success: false,
        data: null,
        message: [error.message]
      });
    });

  },
  login(req, res) {
    //console.log("#req: "+JSON.stringify(req.body))
    return User.find({
      where: {email: req.param('email', null)}
    }).then((data) => {
      bcrypt.compare(req.param('password', null), data.password, (err, isMatch) => {
        if (err) return (err);
        if (!isMatch) {
          console.log("failed.....");
          return res.status(200).json({
            success: false,
            data: null,
            message: ['la clave no coincide']
          });
        }
        let payload = {
          'username': data.name,
          'email': data.email,
          'id': data.id,
          'exp': moment().add(14, "days").unix()
        };

        let token = jwt.encode(payload, process.env.APP_KEY);

        res.status(200).json({
          success: true,
          data: {
            token: token,
            refresh: null
          },
          message: 'ok'
        });

      });

    }).catch((error) => {
      res.status(200).send({
        success: false,
        data: null,
        message: [error.message]
      })
    });
  },
  forgot(req, res) {
    return User.find({
      where: {email: req.param('email', null)}
    }).then((data) => {
      if (data === undefined || data == null) {
        res.status(200).send({
          success: false,
          data: null,
          message: 'invalid email'
        })
      } else {
        let r = Math.random().toString(36).substring(3);
        let remember_token = r;
        return data.update({
          remember_token: remember_token
        }).then((up) => {
          res.status(200).json({
            success: true,
            data: up,
            message: 'ok'
          });
        }).catch((error) => {
          res.status(200).send({
            success: false,
            data: null,
            message: error.message
          });
        });
      }
    }).catch((error) => {
      res.status(200).send({
        success: false,
        data: null,
        message: error.message
      });
    });
  },
  resetPassw(req, res) {
    let password = req.body.password;
    return User.find({
      where: {remember_token: req.param('remember_token', null)}
    }).then((data) => {
      if (data === undefined || data == null) {
        res.status(200).send({
          success: false,
          data: null,
          message: 'null remember token'
        })
      } else {
        bcrypt.genSalt(saltRounds, (err, salt) => {
          if (err) return next(err);

          bcrypt.hash(password, salt, (err, hash) => {
            if (err) return next(err);
            password = hash;

            return data.update({
              password: password
            }).then((up) => {
              res.status(200).json({
                success: true,
                data: up,
                message: 'ok'
              });
            }).catch((error) => {
              res.status(200).send({
                success: false,
                data: null,
                message: error.message
              });
            });

          });
        });
      }
    });
  },
  create(req, res) {
    let password = req.param('password', null);
    let email = req.param('email', null);
    let name = req.param('name', null);
    User.find({where: {email: email}}).then((data) => {
      if (data === undefined || data === null) {
        bcrypt.genSalt(saltRounds, (err, salt) => {
          if (err) return next(err);

          bcrypt.hash(password, salt, (err, hash) => {
            if (err) return next(err);
            password = hash;
            let payload = {
              name: name,
              email: email,
              password: password
            };

            return User.create(payload).then((data) => {
              res.status(200).json({
                success: true,
                data: data,
                message: 'ok'
              });
            }).catch((error) => {
              res.send({
                success: false,
                data: null,
                message: error.message
              });
            });

          });
        });
      } else {
        res.send({
          success: false,
          data: null,
          message: 'recorset isset'
        });
      }
    }).catch((error) => {
      res.send({
        success: false,
        data: null,
        message: error.message
      });
    });

  },
  list(req, res) {
    return User.findAll({attributes: ['id', 'name', 'email']}).then((data) => {
      res.status(200).send({
        success: true,
        data: data,
        message: 'ok'
      })
    }).catch((error) => {
      res.status(200).send({
        success: false,
        data: null,
        message: error.message
      })
    })
  },
  edit(req, res) {
    return User.find({
      attributes: ['id', 'name', 'email'],
      where: {id: req.params.id}
    }).then((data) => {
      res.status(200).json({
        success: true,
        data: data,
        message: 'ok'
      });
    }).catch((error) => {
      res.status(200).send({
        success: false,
        data: null,
        message: [error.message]
      })
    });
  },
  update(req, res, next) {
    try {
      let password = req.param('password', null);
      let email = req.param('email', null);
      let name = req.param('name', null);

      return User.find({
        where: {id: req.params.id}
      }).then((data) => {
        if (!data) {
          res.status(200).send({
            success: false,
            data: null,
            message: ['null']
          })
        }

        if (data !== null) {
          if(password!==null) {
            bcrypt.genSalt(saltRounds, (err, salt) => {
              if (err) return next(err);

              bcrypt.hash(password, salt, (err, hash) => {
                if (err) return next(err);
                password = hash;
                return data.update({
                  name: name,
                  email: email,
                  password: password
                }).then((up) => {
                  res.status(200).json({
                    success: true,
                    data: up,
                    message: 'ok'
                  });
                }).catch((error) => {
                  res.status(200).send({
                    success: false,
                    data: null,
                    message: [error.message]
                  });
                });

              });
            });
          }else{
            res.status(200).send({
              success: false,
              data: null,
              message: ['se requiere la clave']
            })
          }

        } else {
          res.status(200).send({
            success: false,
            data: null,
            message: ['no update']
          })
        }


      }).catch((error) => {
        res.status(200).send({
          success: false,
          data: null,
          message: [error.message]
        })
      });
    } catch (e) {
      res.status(200).send({
        success: false,
        data: null,
        message: [e.message]
      })
    }
  },
  delete(req, res, next) {
    return User.find({
      where: {id: req.params.id}
    }).then((data) => {
      if (!data) {
        res.status(200).send({
          success: false,
          data: null,
          message: 'null'
        })
      }

      if (data !== null) {
        return data.destroy().then((des) => {
          res.status(200).json({
            success: true,
            data: des,
            message: 'ok'
          });
        }).catch((error) => {
          res.status(200).send({
            success: false,
            data: null,
            message: [error.message]
          });
        });
      } else {
        res.status(200).send({
          success: false,
          data: null,
          message: ['null']
        })
      }

    }).catch((error) => {
      res.status(200).send({
        success: false,
        data: null,
        message: [error.message]
      })
    });
  }
};