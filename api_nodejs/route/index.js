'use strict'
const UserController = require('../controller').User

const middle = require('../middleware').middleware;

module.exports = (app) => {
  const api = '/api/';

  app.get('/', (req, res) => {
    res.header('Content-type', 'text/html');
    return res.end('<h1>Hello, Secure World!</h1>');
  });

  app.get(api + 'user/index', UserController.index);

  app.post(api + 'login', UserController.login);

  app.post(api + 'register', UserController.register);

  app.post(api + 'forgot-password', UserController.forgot);

  app.post(api + 'reset-password', UserController.resetPassw);

  app.get(api + 'auth', UserController.auth);

  app.post(api + 'user/create', middle.auth, UserController.create);

  app.get(api + 'user/all', middle.auth, UserController.list);

  app.get(api + 'user/edit/:id', middle.auth, UserController.edit);

  app.put(api + 'user/update/:id', middle.auth, UserController.update);

  app.get(api + 'user/delete/:id', middle.auth, UserController.delete);

};