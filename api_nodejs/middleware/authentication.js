'use strict'

const User = require('../model').users;

const moment = require('moment');
const jwt = require('jwt-simple');

module.exports = {


  auth(req, res, next) {
    if (!req.headers.authorization) {
      return res
        .status(403)
        .send({success: false, data: null, message: "Tu petición no tiene cabecera de autorización"});
    }

    var token = req.headers.authorization.split(" ")[1];
    var payload;
    try {
      payload = jwt.decode(token, process.env.APP_KEY);
    } catch (e) {
      payload = null;
      return res
        .status(403)
        .send({success: false, data: null, message: "Token incorrecto"});
    }


    try {
      if (payload.exp <= moment().unix()) {
        return res
          .status(401)
          .send({success: false, data: null, message: "El token ha expirado"});
      }

      User.find({where: {email: payload.email}}).then((data) => {
        if (data === null) {
          return res
            .status(403)
            .send({success: false, data: null, message: "Incorrect credentials"});
        } else {
          next();
        }
      });

    } catch (err) {
      console.log('#err: ' + err.message)
      next();
    }
  }

}